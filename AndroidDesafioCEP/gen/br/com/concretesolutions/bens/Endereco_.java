//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.com.concretesolutions.bens;

import android.content.Context;

public final class Endereco_
    extends Endereco
{

    private Context context_;

    private Endereco_(Context context) {
        context_ = context;
        init_();
    }

    public static Endereco_ getInstance_(Context context) {
        return new Endereco_(context);
    }

    private void init_() {
    }

    public void rebind(Context context) {
        context_ = context;
        init_();
    }

}
