package br.com.concretesolutions.tasks;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.rest.RestService;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

import android.app.ProgressDialog;
import br.com.concretesolutions.activity.MainActivity;
import br.com.concretesolutions.bens.Endereco;
import br.com.concretesolutions.utils.Constantes;
import br.com.concretesolutions.utils.Mask;
import br.com.concretesolutions.webservice.WebService;

@EBean
public class ThreadBuscarCEP {

	@Bean
	Endereco endereco;

	@RootContext
	protected MainActivity context;

	@RestService
	protected WebService webService;

	private ProgressDialog progress;

	@Background
	public void doSomethingInBackground(String cep) {
		mostrarCarregar();
		try {
			endereco = webService.obterCEP(Mask.unmask(cep));
		} catch (RestClientException e) {
			String mensagem = Constantes.ERRO_BUSCAR_CEP;
			if(e instanceof HttpClientErrorException){
				HttpStatus httpStatus =  ((HttpClientErrorException) e).getStatusCode();
				if (httpStatus == HttpStatus.SERVICE_UNAVAILABLE)
					mensagem = Constantes.ERRO_SERVICO_INDISPONIVEL;
				else if (httpStatus == HttpStatus.NOT_FOUND)
					mensagem = Constantes.ERRO_CEP_NAO_ENCONTRADO;
			}else if(e instanceof ResourceAccessException){
				mensagem = Constantes.ERRO_SEM_CONEXAO;
			}
			endereco = null;
			mensagemTela(mensagem);
		}
		if (endereco != null){
			atualizarTela(endereco);
			context.salvaHistorico(cep);
		}
		fecharCarregar();
	}

	private void mensagemTela(String msg) {
		context.mensagemTela(msg);
	}

	private void atualizarTela(Endereco endereco) {
		context.atualizarTela(endereco);
	}

	@UiThread
	void mostrarCarregar() {
		progress = new ProgressDialog(context);
		progress.setMessage(Constantes.STRING_AGUARDE);
		progress.setCancelable(false);
		progress.show();
	}

	@UiThread
	void fecharCarregar() {
		progress.dismiss();
	}
}
