package br.com.concretesolutions.webservice;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.RestClientErrorHandling;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import br.com.concretesolutions.bens.Endereco;
import br.com.concretesolutions.utils.Constantes;

@Rest(rootUrl = Constantes.URL_PADRAO, converters = { GsonHttpMessageConverter.class })
public interface WebService extends RestClientErrorHandling {
	
	@Get(Constantes.URL_CEP)
	Endereco obterCEP(String cep);
}