package br.com.concretesolutions.bens;

import org.androidannotations.annotations.EBean;

import com.google.gson.annotations.SerializedName;

@EBean
public class Endereco {
	
    @SerializedName("estado")
    public String estado;
    
    @SerializedName("cidade")
    public String cidade;
    
    @SerializedName("bairro")
    public String bairro;

    @SerializedName("tipoDeLogradouro")
    public String tipoLogradouro;

    @SerializedName("logradouro")
    public String logradouro;

    public String getRua(){
    	return this.tipoLogradouro+ " " + this.logradouro;
    }
    
    public String getLocalidade(){
    	return this.cidade+ "-" + this.estado;
    }

	public String getBairro() {
		return bairro;
	}
}