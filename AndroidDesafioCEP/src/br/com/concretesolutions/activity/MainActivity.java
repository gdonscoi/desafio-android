package br.com.concretesolutions.activity;

import java.util.Map;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import br.com.concretesolutions.bens.Endereco;
import br.com.concretesolutions.tasks.ThreadBuscarCEP;
import br.com.concretesolutions.utils.Constantes;
import br.com.concretesolutions.utils.Mask;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {

	private SharedPreferences sharedpreferences;

	@ViewById
	protected EditText cep;

	@ViewById
	public TextView text_logradouro;

	@ViewById
	public TextView text_bairro;

	@ViewById
	public TextView text_localidade;

	@Bean
	protected ThreadBuscarCEP threadBuscarCEP;

	@AfterViews
	protected void setMask() {
		cep.addTextChangedListener(Mask.insert("##.###-###", cep));
		sharedpreferences = getSharedPreferences(Constantes.STRING_HISTORICO,
				Context.MODE_PRIVATE);
	}

	@Click({ R.id.botaoBuscar })
	public void clickBuscarCEP() {
		threadBuscarCEP.doSomethingInBackground(cep.getText().toString());
	}

	@Click(R.id.botaoHistorico)
	public void clickHistorico() {
		final CharSequence[] listaHistorico = carregarHistorico();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.botao_historico).setItems(listaHistorico,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						cep.setText("");
						cep.setText(listaHistorico[which].toString());
						clickBuscarCEP();
						dialog.dismiss();
					}
				});
		builder.create().show();
	}

	@UiThread
	public void atualizarTela(Endereco endereco) {
		text_logradouro.setText(endereco.getRua());
		text_bairro.setText(endereco.getBairro());
		text_localidade.setText(endereco.getLocalidade());
		((InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE))
				.hideSoftInputFromWindow(cep.getWindowToken(), 0);
	}

	@UiThread
	public void mensagemTela(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}

	@UiThread
	protected void limparTela() {
		text_logradouro.setText("");
		text_bairro.setText("");
		text_localidade.setText("");
	}

	public CharSequence[] carregarHistorico() {
		Map<String, ?> keys = sharedpreferences.getAll();
		return keys.values().toArray(new CharSequence[keys.size()]);
	}

	public void salvaHistorico(String cep) {
		Editor editor = sharedpreferences.edit();
		editor.putString(Mask.unmask(cep), cep);
		editor.commit();
	}
}
