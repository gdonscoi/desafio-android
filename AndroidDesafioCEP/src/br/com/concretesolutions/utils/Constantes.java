package br.com.concretesolutions.utils;

public interface Constantes {

	public static String ERRO_BUSCAR_CEP = "Erro ao buscar CEP.";
	public static String ERRO_SERVICO_INDISPONIVEL = "Servi�o Indispon�vel.";
	public static String ERRO_CEP_NAO_ENCONTRADO = "CEP n�o encontrado.";
	public static String ERRO_SEM_CONEXAO = "Verifique sua conex�o com a internet.";

	public static String STRING_AGUARDE = "Aguarde...";
	public static String STRING_HISTORICO = "historico";

	public static String URL_PADRAO = "http://correiosapi.apphb.com";
	public static String URL_CEP = "/cep/{cep}";
	
}
